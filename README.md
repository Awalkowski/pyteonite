# Teonite blog scraper
This is a API for Teonite blog which can be found here: https://teonite.com/blog/

Scraped articles are being filtered against the list of english stopwords.

## API endpoints:
    /stats/ - returns 10 most common words from all blog articles
    /stats/{author_unidecode_name} - returns 10 most common words for given author
    /authors/ - returns a list of authors

## How to run:
* clone repository
* navigate to the root folder (pyteonite)
* run following commands:
** docker-compose run web python ./manage.py migrate  (this will take a while)
** docker-compose up

## Clean database
* docker-compose run web python ./manage.py flush

## TESTS:
To run tests simply run this command:
* docker-compose run web python ./manage.py test