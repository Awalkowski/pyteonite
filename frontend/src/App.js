import React from 'react';

class App extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            items: {},
            isLoaded: false,
        }
    }

    componentDidMount() {
        fetch('http://localhost:8070/stats/')
        .then(res => res.json())
        .then(res => {
            console.log(res);
            this.setState({
                isLoaded: true,
                items: res,
            })
        });
    }

    render(){

         const { isLoaded, items } = this.state;

        if (!isLoaded){
            return <div>Loading ... </div>
        }
        else {

        return(
            <div className="App">
                        Data has been Loaded
                         {JSON.stringify(this.state.items)}
            </div>
        );
        }
    }
}

export default App;