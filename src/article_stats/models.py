from django.db import models


class Author(models.Model):
    full_name = models.CharField(max_length=100)
    unidecode_name = models.CharField(default="", max_length=100)

    def __str__(self):
        return self.full_name


class Article(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=200)
    text = models.TextField()

    def __str__(self):
        return self.title
