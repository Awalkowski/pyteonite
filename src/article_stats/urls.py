from rest_framework import routers
from .views import ArticleViewSet, AuthorViewSet

router = routers.DefaultRouter()
router.register('stats', ArticleViewSet, 'statistics')
router.register('authors', AuthorViewSet, 'authors')

urlpatterns = router.urls