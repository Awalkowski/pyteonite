import logging
import re

from urllib.request import Request, urlopen
from urllib import error
from bs4 import BeautifulSoup
from unidecode import unidecode
from article_stats.models import Article
from article_stats.models import Author

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Parser:
    URL = "https://teonite.com/"
    BLOG_SUFFIX = "blog/"
    BLOG_PAGES = 0
    BLOG_URLS = []
    HEADERS = {'User-Agent': 'Mozilla/5.0'}
    FULL_BLOG_URL = f"{URL}{BLOG_SUFFIX}"

    @staticmethod
    def get_soup_handler(url, headers=HEADERS):
        logger.info(f"Requesting : {url}")
        req = Request(url, headers=headers)
        try:
            with urlopen(req) as response:
                soup = BeautifulSoup(response, features="lxml")
                return soup
        except error.URLError as e:
            logger.error(f"ERROR: {e} occured for URL : {url}")
            return None

    def _get_nmbr_of_blog_pages(self):
        soup = self.get_soup_handler(f"{self.URL}{self.BLOG_SUFFIX}", self.HEADERS)
        if soup:
            pages = soup.find("span", class_="page-number").get_text()
            pages = re.split('/', pages)[-1]
            return pages
        else:
            return None

    def scrap_article(self, article_url):
        soup = self.get_soup_handler(article_url, self.HEADERS)
        if soup:
            text = soup.find("div", class_="post-content").get_text()
            full_name = soup.find("span", class_="author-name").get_text()
            unidecode_author = unidecode(full_name.lower().replace(" ", ""))
            title = soup.find("h1", class_="post-title").get_text()
            text = text.replace('\n', '')
            author, _ = Author.objects.get_or_create(full_name=full_name,
                                                     unidecode_name=unidecode_author)
            article = Article.objects.create(
                author=author,
                title=title,
                text=text,
            )
            article.save()
            logger.info(f"Article scraped : {title}")
        else:
            return None

    @staticmethod
    def _scrap_blog_urls(soup):
        posts_urls_raw = soup.find_all("h2", class_="post-title")
        links = [
            link.find('a').get('href').split("../")[-1]
            for link in posts_urls_raw
        ]
        return links

    def _request_for_given_blog_page(self, page_url):
        soup = self.get_soup_handler(page_url, self.HEADERS)
        if soup:
            return self._scrap_blog_urls(soup)
        else:
            return None

    def get_from_teonite(self):
        urls = self._request_for_given_blog_page(self.FULL_BLOG_URL)
        if urls:
            self.BLOG_URLS.append(urls)
            self.BLOG_PAGES = self._get_nmbr_of_blog_pages()
            for page in range(2, int(self.BLOG_PAGES)):
                page_url = f"{self.URL}blog/page/{page}/"
                blog_urls = self._request_for_given_blog_page(page_url)
                self.BLOG_URLS.append(blog_urls)
            self._merge_articles_lists()

    def _merge_articles_lists(self):
        flat_list = [item for sublist in self.BLOG_URLS for item in sublist]
        self.BLOG_URLS = flat_list
