import logging

from django.shortcuts import get_list_or_404
from rest_framework.decorators import action
from .models import Article, Author
from rest_framework.response import Response
from rest_framework import viewsets, permissions
from .serializers import ArticleSerializer, AuthorSerializer
from .utils.utils import sanitize_text, get_most_common_words

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

AUTHOR_REGEX = '(?P<author>[A-Za-z]{0,30})'


class ArticleViewSet(viewsets.ModelViewSet):
    queryset = Article.objects.all()

    permissions_classes = [
        permissions.AllowAny
    ]

    serializer_class = ArticleSerializer

    def list(self, request, *args, **kwargs):
        whole_words = ' '.join([str(i.text) for i in self.queryset])
        filtered_text = sanitize_text(whole_words)
        reseponse_data = get_most_common_words(filtered_text)
        return Response(reseponse_data)

    @action(
        methods=['get'],
        detail=False,
        url_path=f'{AUTHOR_REGEX}',
    )
    def stats(self, *args, **kwargs):
        author = kwargs.get('author')
        author_articles = get_list_or_404(Article, author__unidecode_name=author)
        whole_words = ' '.join([str(i.text) for i in author_articles])
        filtered_text = sanitize_text(whole_words)
        reseponse_data = get_most_common_words(filtered_text)
        return Response(reseponse_data)


class AuthorViewSet(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    permissions_classes = [
        permissions.AllowAny
    ]

    def list(self, request, *args, **kwargs):
        authors = Author.objects.all()
        serializer = AuthorSerializer(authors, many=True)
        return Response(serializer.data)
