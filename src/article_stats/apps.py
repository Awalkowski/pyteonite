from django.apps import AppConfig


class ArticleStatsConfig(AppConfig):
    name = 'article_stats'
