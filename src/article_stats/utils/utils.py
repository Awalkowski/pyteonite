import collections
import re


def sanitize_text(text):
    with open('english_stopwords.txt', 'r') as f:
        stop_words = f.readlines()
    target_words = re.findall(r'[\w]+', text.lower())
    stop_words = set(map(str.strip, stop_words))
    filtered_text = [w for w in target_words if w not in stop_words]
    return filtered_text


def get_most_common_words(text):
    interesting_word_counts = collections.Counter(text)
    most_common = interesting_word_counts.most_common(10)
    dict_repsonse = {word[0]: word[1] for word in most_common}
    return dict_repsonse
