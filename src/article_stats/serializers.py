from rest_framework import serializers
from rest_framework.serializers import Serializer
from .models import Article
from .models import Author


class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ('author', 'title', 'text')


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ('full_name', 'unidecode_name')

    def to_representation(self, instance):
        custom_resp = {instance.full_name: instance.unidecode_name}
        return custom_resp
