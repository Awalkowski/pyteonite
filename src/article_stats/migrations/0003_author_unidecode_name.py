# Generated by Django 3.0 on 2019-12-21 09:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('article_stats', '0002_remove_author_unidecode_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='author',
            name='unidecode_name',
            field=models.CharField(default='', max_length=100),
        ),
    ]
