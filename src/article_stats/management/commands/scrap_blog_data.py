import logging
from django.core.management.base import BaseCommand
from ...WebScraper import Parser

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Scrap all blog posts from Teonite'

    def add_arguments(self, parser):
        parser.add_argument(
            '--articles',
            type=int,
            required=False,
            help='Number of articles to scrap',
        )

    def handle(self, *args, **options):
        parser = Parser()
        parser.get_from_teonite()

        nmbr_to_scrap = options['articles'] if options else len(parser.BLOG_URLS)

        for i in parser.BLOG_URLS[:nmbr_to_scrap]:
            article_url = f"{parser.URL}{i}"
            parser.scrap_article(article_url)
        self.stdout.write(self.style.SUCCESS('Successfully scraped blog data'))
