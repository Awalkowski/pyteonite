from urllib import error

from django.test import TestCase
from article_stats.WebScraper import Parser

EXAMPLE_BLOG_URL = "blog/top-10-artificial-intelligence-image-processing-apps/"


class ScraperTest(TestCase):
    parser = Parser()

    def test_get_nmbr_pages_returns_correct_value(self):
        nmbr = self.parser._get_nmbr_of_blog_pages()
        self.assertGreater(int(nmbr), 1)

    def test_scraper_bad_url_request(self):
        bad_url = "https://badurl.asdas"
        self.assertIsNone(self.parser.get_soup_handler(bad_url))

    def test_scraper_successful_blog_urls(self):
        soup = self.parser.get_soup_handler(f"{self.parser.URL}{self.parser.BLOG_SUFFIX}")
        urls = self.parser._scrap_blog_urls(soup)
        self.assertGreater(len(urls), 1)
        self.assertIn(EXAMPLE_BLOG_URL, urls)
