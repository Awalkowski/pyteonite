from io import StringIO
from django.core.management import call_command
from django.test import TestCase


class ScrapBlogCommandTest(TestCase):
    def test_scraper_comand_successful_run(self):
        out = StringIO()
        call_command('scrap_blog_data', f'--articles={1}', stdout=out)
        self.assertIn('Successfully scraped blog data', out.getvalue())

    def test_scraper_command_no_arguments(self):
        out = StringIO()
        call_command('scrap_blog_data', stdout=out)
        self.assertIn('Successfully scraped blog data', out.getvalue())