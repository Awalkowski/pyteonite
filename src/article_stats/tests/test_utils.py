from django.test import TestCase
from article_stats.utils import utils

EXAMPLE_TEXT = "I like Teonite I think they are cool, like like"


class ScraperTest(TestCase):

    def test_successful_removed_stopwords(self):
        filtered_text = utils.sanitize_text(EXAMPLE_TEXT)
        expected_result = {'like', 'like', 'like', 'teonite', 'think', 'cool'}
        self.assertSetEqual(set(filtered_text), expected_result)

    def test_successful_count_of_common_words(self):
        filtered_text = utils.sanitize_text(EXAMPLE_TEXT)
        most_common_words = utils.get_most_common_words(filtered_text)
        expected_result = {'like': 3, 'teonite': 1, 'think': 1, 'cool': 1}
        self.assertEqual(most_common_words, expected_result)
