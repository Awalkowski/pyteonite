from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from article_stats.models import Author, Article
from model_mommy import mommy
from .conftest import client


class ScraperTest(TestCase):

    def setUp(self):
        """Setup for tests to seed db with a test data"""
        self.author1 = mommy.make(Author, full_name="Test_user", unidecode_name='testuser')
        self.author2 = mommy.make(Author, full_name="Test_user2", unidecode_name='testuser2')
        self.article1 = mommy.make(Article, author=self.author1, title='test title',
                                   text='asd third')
        self.article2 = mommy.make(Article, author=self.author2, title='test', text='asd test')

    def test_api_stats_correct_response(self):
        response = client().get(
            reverse('statistics-list'))
        expected_result = {'asd': 2, 'third': 1, 'test': 1}
        self.assertEqual(response.data, expected_result)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_api_authors_correct_response(self):
        response = client().get(
            reverse('authors-list'))
        expected_result = [{self.author1.full_name: self.author1.unidecode_name},
                           {self.author2.full_name: self.author2.unidecode_name}]
        self.assertEqual(response.data, expected_result)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_api_stats_per_author_correct_response(self):
        response = client().get(
            reverse('statistics-stats', args=(self.author1.unidecode_name,)))
        expected_result = {'asd': 1, 'third': 1}
        self.assertEqual(response.data, expected_result)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_api_stats_for_non_existing_author(self):
        response = client().get(
            reverse('statistics-stats', args=("nonauthor",)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
